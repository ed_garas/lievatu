var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var sourceMap = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var minify = require('gulp-minify-css');

var jsFiles = ['wp-content/themes/lievatu/js/**/*.js'];
var phpFiles = ['wp-content/themes/lievatu/**/*.php'];
var scssFiles = [
    'wp-content/themes/lievatu/scss/normalize.css',
    'wp-content/themes/lievatu/scss/bootstrap.min.css',
    'wp-content/themes/lievatu/scss/bootstrap-theme.min.css',
    'wp-content/themes/lievatu/scss/**/*.scss'
];


gulp.task('server', ['sass'], function () {
    browserSync.init({
        proxy: "http://lievatu.dev/",
        // proxy: "http://lievatu.lt/",
        open: false,
        reloadOnRestart: true
    });

    gulp.watch(scssFiles, ['sass']);
    gulp.watch(jsFiles).on('change', browserSync.reload);
    gulp.watch(phpFiles).on('change', browserSync.reload);
});
gulp.task('sass', function () {
    return gulp.src(scssFiles)
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourceMap.write())
        .pipe(concat('main.css'))
        .pipe(gulp.dest("wp-content/themes/lievatu/scss"))
        .pipe(browserSync.stream({match: '**/*.css'}));
});
gulp.task('default', ['server']);