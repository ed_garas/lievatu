

var APP = function () {};
var Menu = function () {};
var Promise = function () {};


Menu.prototype = new APP();

APP.prototype.toggle = function (trigger, containerToOpen, slideSpeed) {
    $(trigger).on('click', function () {
        $(this).toggleClass('active');
        $(containerToOpen).slideToggle(slideSpeed);
    });
};



var MenuManager = new Menu();
MenuManager.toggle('.toggle_menu', '#menu_container', 150);


Promise.prototype.openVideo = function (trigger, container, containerToOpen) {
    $(trigger).on('click', function () {
        $(this).toggleClass('active');
        $(this).closest(container).find(containerToOpen).slideToggle(150);
        //$(containerToOpen).slideToggle(150);
    });
};

var promise = new Promise();
promise.openVideo('.video_trigger', 'article', '.promise_video');