<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package lievatu
 */

get_header(); ?>
	<div id="primary" class="content-area col-md-12">

		<?php
		if ( have_posts() ) : ?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
                get_template_part('template-parts/promises/content-promise', 'page');

                comments_template();

			endwhile; ?>

            <?php
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

    </div><!-- #primary -->

<?php if (function_exists(custom_pagination)): ?>
    <div class="col-xs-12">
        <?php custom_pagination($loop->max_num_pages,"",$paged); ?>
    </div>
<?php endif; ?>

<?php
//get_sidebar();
get_footer();
