<?php
/* Template Name: Promise list */
get_header();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'promise',
    'posts_per_page' => -1,
    'paged' => $paged
);
$loop = new WP_Query($args);
?>
    <div id="primary" class="content-area col-md-12">

        <?php while ($loop->have_posts()) : $loop->the_post();
            get_template_part('template-parts/promises/content-promise', 'page');
        endwhile; // End of the loop. ?>

    </div><!-- #primary -->

<?php if (function_exists(custom_pagination)): ?>
    <div class="col-xs-12">
        <?php custom_pagination($loop->max_num_pages,false,$paged); ?>
    </div>
<?php endif; ?>

<?php
get_footer();
