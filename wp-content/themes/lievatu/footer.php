<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lievatu
 */

?>

	</div><!-- #content, row -->


<div class="row">
    <footer id="footer" class="col-md-12">
        <div class="site-info">
            <a target="_blank" href="http://robotboyz.com">Made by: robotboyz</a>
            <a href="/apie-mus">Apie mus</a>
            <a href="/partneriai">Partneriai</a>
        </div><!-- .site-info -->
    </footer><!-- #colophon -->
</div>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
