<?php
/* Template Name: Promise by status */

get_header();

$status_id = null;
if(!empty($_GET)){
    $status_id = $_GET['status'];
}

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'promise',
    'posts_per_page' => 30,
    'paged' => $paged,
    'meta_query'     => array(
        array(
            'key'     => 'wpcf-pazado-statusas',
            'value' => $status_id
        )
    )
);
$loop = new WP_Query($args);


?>
    <div id="primary" class="content-area col-md-12">

        <?php if($loop->have_posts()): ?>
        <?php while ($loop->have_posts()) : $loop->the_post();
            get_template_part('template-parts/promises/content-promise', 'page');
        endwhile; // End of the loop. ?>
        <?php endif; ?>

    </div><!-- #primary -->
<?php if (function_exists(custom_pagination)): ?>
    <div class="col-xs-12">
        <?php custom_pagination($loop->max_num_pages,"",$paged_party); ?>
    </div>
<?php endif; ?>

<?php
get_footer();
