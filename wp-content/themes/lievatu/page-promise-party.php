<?php
/* Template Name: Promise by party list */

get_header();

$party_id = null;
if(!empty($_GET)){
    $party_id = $_GET['partija'];
}

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'promise',
    'posts_per_page' => 30,
    'paged' => $paged,
    'meta_query'     => array(
        array(
            'key'     => 'wpcf-partija',
            'value' => $party_id
        )
    )
);
$loop = new WP_Query($args);
?>
    <div id="primary" class="content-area col-md-12">

        <?php if($loop->have_posts()): ?>
            <?php while($loop->have_posts()): ?>
                <?php $loop->the_post() ?>
                <?php get_template_part('template-parts/promises/content-promise', 'page'); ?>


            <?php endwhile; ?>
        <?php else: ?>
            <article>
                Dar nieko nepažadėjo
            </article>
        <?php endif; ?>

    </div><!-- #primary -->
<?php if (function_exists(custom_pagination)): ?>
    <div class="col-xs-12">
        <?php custom_pagination($loop->max_num_pages,"",$paged); ?>
    </div>
<?php endif; ?>

<?php
get_footer();
