<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lievatu
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'lievatu' ); ?></a>


    <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">

            <div class="row row-no-margin">
                <div class="navbar-header">

                    <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>-->

                    <button type="button" class="navbar-toggle collapsed toggle_menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>


                    <a class="navbar-brand" rel="home" href="<?php echo get_home_url(); ?>" title="Lievatu">
                        <img style="max-width:30px; margin-top: -7px; display: inline-block;" src="<?php echo get_template_directory_uri() . '/img/ltu-logo.png'; ?>">
                        <span>LIEVATU</span>
                    </a>
                </div>

                <div id="navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <button type="button" class="navbar-toggle toggle_menu hidden-xs">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </li>
                    </ul>

                    <form class="navbar-form navbar-right hidden-xs" role="search" method="get" id="menu-searchform" action="<?php esc_url( home_url( '/' ) ); ?>">
                        <div class="form-group">
                            <input type="search" class="search-field search-query form-control" autocomplete="off" placeholder="paieška" value="<?php get_search_query() ?>" name="s" />
                        </div>
                    </form>

                </div>
            </div>




            <div id="menu_container" class="row" style="display: none;">
                <div class="col-md-12">
                    <div class="menu-wrap border-top">


                        <div id="parties_wrap">
                            <?php get_parties(); ?>
                        </div>

                        <?php //wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>






    <div id="page" class="container">
	<div id="content" class="row">
