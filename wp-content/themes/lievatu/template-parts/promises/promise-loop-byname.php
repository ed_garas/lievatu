<?php

$main_post_id = get_the_ID();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'promise',
    'posts_per_page' => 30,
    'paged' => $paged,
    'meta_query'     => array(
        array(
            'key'     => 'wpcf-vardas-pavarde',
            'value' => types_render_field( 'vardas-pavarde' )
        )
    )
);
$loop = new WP_Query($args);

?>

<?php while ($loop->have_posts()) : $loop->the_post();
    $post_id = get_the_ID();

    if($main_post_id !== $post_id){
        get_template_part('template-parts/promises/content-promise', 'page');
    }

endwhile; // End of the loop. ?>