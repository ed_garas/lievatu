<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package lievatu
 */
?>

<?php
$meta = get_post_meta( get_the_ID() );

$party = $meta['wpcf-partija'][0];
$getPartyUrl = get_site_url().'/partiju-pazadai/?partija='.$party;

$status = $meta['wpcf-pazado-statusas'][0];
$getStatusUrl = get_site_url().'/pazadai-pagal-statusa/?status='.$status;

?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


    <?php if(types_render_field( 'vaizdo-irasas' ) !== ''): ?>
        <div class="video_trigger">
            <span class="glyphicon glyphicon-film"></span>
        </div>
    <?php endif; ?>


    <div class="promise_header">
        <a href="<?php echo get_post_permalink() ?>" class="person_photo">
            <img src="<?php the_post_thumbnail_url( 'thumbnail-person' ); ?>" alt="" class="img-responsive">
        </a>

        <div class="head_info">
            <a href="<?php echo get_post_permalink() ?>" class="name"><?php echo(types_render_field( 'vardas-pavarde' )); ?></a>
            <div class="party"><a href="<?php echo $getPartyUrl; ?>"><?php echo(types_render_field( 'partija' )); ?></a></div>
            <div class="promise_source">
                <?php echo(types_render_field( 'saltinis' )); ?>
            </div>
        </div>
    </div>

    <div class="promise_body">
        <?php if(types_render_field( 'vaizdo-irasas' ) !== ''): ?>
            <div class="promise_video">
                <?php echo(types_render_field( 'vaizdo-irasas' )); ?>
            </div>
        <?php endif; ?>

        <div class="promise_text">
            <?php the_content(); ?>
        </div>

        <?php $promise_status_id = $meta['wpcf-pazado-statusas'][0]; ?>
        <div class="promise_status <?php echo 'status_'.$promise_status_id ?>">
            <a href="<?php echo $getStatusUrl; ?>">
                <?php echo(types_render_field( 'pazado-statusas' )); ?>
            </a>
        </div>

    </div>


</article>
<?php lievatu_entry_footer(); ?>