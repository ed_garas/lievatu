<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lievatu_web');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l8vb;hQ<*9n+R,k) ag-hge9PDY4P -337jx~}:6`qMVQUS_r!@pf6gU*bY#9XRp');
define('SECURE_AUTH_KEY',  '>*X@@g58TalhII3JUoPZ-j??{ckvx<;@#(qE#c!:&=HE2^Yd_bY45B=a Af@S/=I');
define('LOGGED_IN_KEY',    'pba^E:L8e3{p()Qx9boj+/uF{x :t?!-3/%5UbdRF.%xTw~MPk#D>VB. 3c[o$y}');
define('NONCE_KEY',        '{xL}Q}RR3loK)srb_]Y9<zH9I2vK6:C/.$W bQ[%!Y7qOCW&`_,9u<=%`2-h4SBI');
define('AUTH_SALT',        '%N1<VsoF`tHO4(u:b[Qjl/gN{dg?GVla!8Y~%q]Vo~tW2vaI^o*@myx(~H*Vx#V)');
define('SECURE_AUTH_SALT', 'b4RHJudPxY@Z?q7>1C)7+]VG-]KAeHwyzT.unNq|;)L/&%tu^07g@O~gT9V}eCvJ');
define('LOGGED_IN_SALT',   'u^`-Z4ot+27|lsuT_$#:B|iot(0Jlmn-%AnH~%$+<r+j(r`Eaka_=KL%a6/yn;{P');
define('NONCE_SALT',       'dr9a>^~vCLKFfv,@Uu0sP6olALw]yf9EWd.8WP05PN{{]nqdS/j4S$_V(}x@.z1c');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'robotboy_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
